# Hunter's Super-Cool Hyper-V Backup

########## CONFIG ############

$VMName="Dev" # Name of the VM to backup
$ToKeep=2 # Number of backup directories to preserve
$BaseDir="D:\Backups\HyperV\" # Base directory where the backups will be stored

##############################

$error.clear()
$VM=Get-VM $VMName -ErrorAction SilentlyContinue
if ($error){
    Write-Error ("VM does not exist: "+$VMName)
    return
}
$VMDir=$BaseDir+$VMName+"\"
New-Item -ItemType Directory -Force -Path $VMDir | Out-Null
cd $VMDir
Write-Output "==========================================="
Write-Output "Hunter's Super-Cool Hyper-V Backup starting"
Write-Output "-------------------------------------------"
Write-Output "Checking for excessive existing backups..."
$ExistingBackups = Get-ChildItem | Sort -Descending Name
For ($i=$ToKeep; $i -lt $ExistingBackups.Count; $i++) {
        Write-Output ("Enforcing keep limit, deleting `""+$ExistingBackups[$i].FullName+"`"...")
        Remove-Item -LiteralPath $ExistingBackups[$i].FullName -Force -Recurse
}
$ExportPath = $VMDir + (Get-Date -Format "MMddyyyy") + ".backup"
if (Test-Path $ExportPath) {
    Write-Output "-------------------------------------------"
    Write-Output "Backup for today exists, exiting."
    Write-Output "==========================================="
    return
}
New-Item -ItemType Directory -Force -Path $VMDir | Out-Null
Write-Output ("Starting Export to `""+$ExportPath+"`"...")
Export-VM -Name $VMName -Path $ExportPath
Write-Output "-------------------------------------------"
if(!$error){
    Write-Output "Backup complete."
}
else {
    Write-Output "Backup did not complete successfully."
}
Write-Output "==========================================="

# Hyper-V Backup

Backup Hyper-V as export, can run live.

## Instructions

- Change the config variables in the .ps1 file
- Point Google Backup and Sync at the output directory (if using it for offsite)
- Create scheduled task
  - C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -file c:\path\to\the.ps1
  - Weekly M,T,W,R @ 11:30PM
  
## Caveats

Because the backups are complete exports, the upload size will be the complete
export every time. In my case, its 106GB. Unlimited storage helps, but doesn't
solve slow upload time.